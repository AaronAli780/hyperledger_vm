/*var http = require('http');
http.createServer(function handler(req, res) {
    res.writeHead(200, {'Content-Type': 'text/plain'});
    res.end('Hello World\n');
}).listen(1337, '127.0.0.1');
console.log('Server running at http://127.0.0.1:1337/');
*/

var connect = require('connect');
var serveStatic = require('serve-static');
connect().use(serveStatic("www")).listen(8080, function(){
    console.log('Server running on 8080...');
});